# Nice to meet you
My name's Tai. I'm a software engineer.

- My landing page [here](https://phuctaile.com)
- My resume [here](https://lptai.github.io/resume)
- Download my resume [here](https://github.com/lptai/resume/raw/gh-pages/phuctaile.pdf)
